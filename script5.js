// Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.

// Технічні вимоги:
// + При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. 
// Для цього потрібно надіслати GET запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts

// + Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// Кожна публікація має бути відображена у вигляді картки (приклад в папці), та включати заголовок, текст, 
// а також ім'я, прізвище та імейл користувача, який її розмістив.

// + На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. 
// При натисканні на неї необхідно надіслати DELETE запит 
// на адресу https://ajax.test-danit.com/api/json/posts/${postId}. 

// + Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
// Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, 
// не будуть там збережені. Це нормально, все так і має працювати.

// + Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. 
// При необхідності ви можете додавати також інші класи.

// - Необов'язкові завдання підвищеної складності
// Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження. 
// Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.

// - Додати зверху сторінки кнопку Додати публікацію. При натисканні на кнопку відкривати модальне вікно, 
// в якому користувач зможе ввести заголовок та текст публікації. 

// - Після створення публікації дані про неї необхідно надіслати в POST запиті 
// на адресу: https://ajax.test-danit.com/api/json/posts. 
// Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному порядку). 
// Автором можна присвоїти публікації користувача з id: 1.

// - Додати функціонал (іконку) для редагування вмісту картки. 
// Після редагування картки для підтвердження змін необхідно надіслати PUT запит 
// на адресу https://ajax.test-danit.com/api/json/posts/${postId}.



const wrapper = document.createElement("div"); // створемо контейнер, стилізуємо, додамо клас
wrapper.classList.add("wrapper__cards");
// wrapper.style.cssText = "display: flex; flex-wrap: wrap; justify-content: space-around; padding: 20px;";
document.body.append(wrapper);

const usersUrl = "https://ajax.test-danit.com/api/json/users"; // url для запита на користувачів
const postsUrl = "https://ajax.test-danit.com/api/json/posts"; // url для запита на пости

// // універсальна функція для запитів
// function getRequest(url, method = "GET", config) {
//     return fetch(url, {method: method, ...config}) // повератє promise
// };

// // навішуємо прослуховувач на сторінку для відслідковування моменту завантаження
// document.addEventListener("DOMContentLoaded", function() {
//     const users = getRequest(usersUrl)
//     .then(response => response.json())
//      //без цього кроку з перетворення на json() повертає властивість проміса response, де є статуси, хедери, боді, тощо
//     .then(data => console.log(data));

//     const posts = getRequest(postsUrl)
//     .then(response => response.json())
//     .then(data => console.log(data));

//     return users, posts;
// });

// універсальна функція для запитів (переробила з async/await)
async function getRequest(url, method = "GET", config) {
    const request = await fetch(url, {method: method, ...config});
    return request.json();
};

async function deletePost(postId, callbackRemove) {
    try {
        const response = await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
            method: 'DELETE'
        });
        if (!response.ok) {
            throw new Error('Failed to delete post');
        }
        console.log(`Post with id ${postId} has been deleted successfully`);
        callbackRemove(); // Виклик функції зворотного виклику після успішного видалення (для видалення дом-елемента картки)
    } catch (error) {
        console.error('Error deleting post:', error.message);
    }
}


class UserCard {
    constructor(id, name, email, title, body) {
        this.id = id; // сюди будемо потім прокидувати id поста (post.id)
        this.name = name;
        this.email = email;
        this.title = title;
        this.body = body;
    }

    createCard(container) {
        const divCard = document.createElement("div");
        divCard.classList.add("div__card"); // формуємо пусту карточку
        
        divCard.insertAdjacentHTML("beforeend", `            
            <h3 class="card__title">
                <img class="card__foto" src="#" alt="foto">
                ${this.name}
                <span class="card__email">${this.email}</span>
                <button class="card__button--delete delete_button">&#10006;</button>
            </h3>
            <h4>${this.title}</h4>
            <p>${this.body}</p>
        `) // наповнюємо пусту карточку структурой елементів 

        const deleteButton = divCard.querySelector(".delete_button");

        deleteButton.addEventListener("click", () => {
            deletePost(this.id, () => {
                divCard.remove();
            });
        })
        
        container.append(divCard); // вставляємо карточку в загальний контейнер (обгортку)
    }
}

// навішуємо прослуховувач на сторінку для відслідковування моменту завантаження
document.addEventListener("DOMContentLoaded", async function() {
    const users = await getRequest(usersUrl)
    console.log("Масив користувачів: ", users);  // отримали масив з юзерами

    const posts = await getRequest(postsUrl)
    console.log("Масив усіх постів користувачів: ", posts);  // отримали масив з усіма постами

    users.forEach((user, index) => {
        // const userPosts = posts.filter(post => post.userId === user.id);
        // короткий запис стрілочної функції без () і без {} в один рядок не потребує return

        const userPosts = posts.filter((post) => {
            return post.userId === user.id});
        console.log(`Масив постів користувача id ${index+1}: `, userPosts);  // отримали масиви з постами кожного юзера окремо

        userPosts.forEach((post) => {
            const userCard = new UserCard( // тут порядок параметрів важен!!!, як в клас-конструкторі
                post.id, // Передаємо ідентифікатор поста першим параметром
                user.name, 
                user.email, 
                post.title.charAt(0).toUpperCase()+post.title.slice(1), // замінемо перші літери заголовка і текста на великі
                post.body.charAt(0).toUpperCase()+post.body.slice(1)
            );
            userCard.createCard(wrapper);
        })          
    });
});

